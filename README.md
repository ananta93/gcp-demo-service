# gcp-demo-service Guide

## How to run
Open command prompt or terminal and run  
```python customer_info.py```



## Open postman, create new collection and send the post request

```http://127.0.0.1:5000/customers?customerId=***&name=***&city=***&mobile=***```

Note: Put the value in place of ***

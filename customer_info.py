from flask import Flask
from flask_restful import Resource, Api, reqparse
from google.cloud import datastore
import pandas as pd
import ast
import csv
import random
import schedule
import time

app = Flask(__name__)
api = Api(app)

class Customers(Resource):
    
    def post(self):
        parser = reqparse.RequestParser() 
        parser.add_argument('customerId', required=True)
        parser.add_argument('name', required=True)
        parser.add_argument('city', required=True)
        parser.add_argument('mobile', required=True)
        args = parser.parse_args()

        data = pd.read_csv('customers.csv')
        new_data = pd.DataFrame({
            'customerId': [args['customerId']],
            'name': [args['name']],
            'city': [args['city']],
            'mobile':[args['mobile']]
        })
        data = data.append(new_data, ignore_index=True)
        data.to_csv('customers.csv', index=False) 
        return {'data': data.to_dict()}, 200

        # datastore_client = datastore.Client(project="myproject-12345")
        # entity = datastore.Entity(key=datastore_client.key('CustomerKind'))
        # entity.update({
        #     'customerId': [args['customerId']],
        #     'name': [args['name']],
        #     'city': [args['city']],
        #     'mobile': [args['mobile']]
        # })
        # datastore_client.put(entity)

    def get_customer():
        file = open("customers.csv")
        csvreader = csv.reader(file)
        header = next(csvreader)
        rows = []
        for row in csvreader:
            rows.append(row)
        
        chosen_row = random.choice(list(rows))
        print(chosen_row)
        file.close()
    
    schedule.every(60).minutes.do(get_customer)
    while 1:
        schedule.run_pending()
        time.sleep(1)

api.add_resource(Customers, '/customers') 

if __name__ == '__main__':
    app.run()